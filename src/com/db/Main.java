package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {

	public static void main(String[] args) {
		
		insertion insert=new insertion();
		employeeLogTable logTable=new employeeLogTable();
		deleteFromTable del=new deleteFromTable();
		displayDetails display=new displayDetails();
		salarySlip salaryDetails=new salarySlip();
		updateLogTable updatedetails1=new updateLogTable();
		procedure storeProcedure=new procedure();
		
		//createEmployeeTable();
		//insert.addEmployee();
		//updatedetails1.createLogtable();
		//updatedetails1.updateLog();
		//display.displayEmployee();
		//display.displayTable();
		//salaryDetails.Slip();
		//del.deleteItem();
		//logTable.employeeLog();
		//storeProcedure.procedureUpdate();

	}
	
	public static void createEmployeeTable()
	{
		try {
			//Class.forName("com.mqsql.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test","root","Sarvesh@123");
			Statement stmt=con.createStatement();
			stmt.executeUpdate("create table employee(Ecode int(4) primary key,Ename varchar(20) not null,Designation varchar(4) not null check(Designation in ('SE','SSE','SS','SSS')),Age int(2) not null check(Age>18 and Age<80),Basic_Pay decimal(8,2) not null check(Basic_Pay>6000));");
			System.out.println("Table created");
			con.close();
		}catch(Exception e) {
			System.out.println("error in main page");
		}
	}
	
	

}
