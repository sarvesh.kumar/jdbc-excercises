package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Scanner;

public class updateLogTable {
	
	public void createLogtable()
	{
		try {
			//Class.forName("com.mqsql.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test","root","Sarvesh@123");
			Statement stmt=con.createStatement();
			stmt.executeUpdate("create table updatelog(modification_id int(4) primary key,date_of_modification varchar(20) not null,modification_detail varchar(100));");
			System.out.println("Table created");
			con.close();
		}catch(Exception e) {
			System.out.println("error in create updatelog method");
		}
	}
	
	public void updateLog()
	{
		try {
			Connection con=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test","root","Sarvesh@123");
			Statement stmt=con.createStatement();
			Scanner sc1=new Scanner(System.in);
			System.out.println("-------welcome to update function------");
			System.out.println("Enter new Designation of the employee: ");
			String designation=sc1.nextLine();
			System.out.println("Enter the new Salary: ");
			int sal=sc1.nextInt();
			System.out.println("Enter employee Ecode : ");
			int id=sc1.nextInt();
			stmt.executeUpdate("update employee set Designation='"+designation+"',Basic_Pay='"+sal+"' where Ecode='"+id+"'");
			System.out.println("one row updated please update updateLog table");
			Scanner sc3=new Scanner(System.in);
			System.out.println("Enter modification details : ");
			String details=sc3.nextLine();
			System.out.println("Enter date of modification in format of YYYY-MM-DD : ");
			String date=sc3.nextLine();
			System.out.println("Enter modification id :");
			int update_id=sc3.nextInt();
			stmt.executeUpdate("insert into updatelog(modification_id,date_of_modification,modification_detail) values ('"+update_id+"','"+date+"','"+details+"')");
			System.out.println("one row inserted to updatelog table");
			sc1.close();
			sc3.close();
			con.close();
		}catch(Exception e) {
			System.out.println("something is wrong with updatelog table");
		}
	}

}
